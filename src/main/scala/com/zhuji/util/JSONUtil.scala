package com.zhuji.util

import com.fasterxml.jackson.core.`type`.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper

import scala.collection.mutable

/**
  * @author zhuji on 2019-01-21
  */

object JSONUtil {

  lazy val mapper = new ObjectMapper() with ScalaObjectMapper
  mapper.registerModule(new DefaultScalaModule())

  /**
    * convert to json string with jackson
    */
  def toJsonString(value: Any): String = {
    mapper.writeValueAsString(value)
  }

  def parseObject[T](jsonStr: String, typeReference: TypeReference[T]): T = {
    mapper.readValue(jsonStr, typeReference)
  }

}

object Test extends App {

  import scala.beans.BeanProperty

  case class Test(@BeanProperty i: Int, @BeanProperty j: String)

  val test = Test(1, "j")
  val map = Map("a" -> 1, "b" -> 2)
  val map1 = mutable.Map("a" -> 1, "b" -> 2)
  val tuple = ("t1", "t2")

  val t = JSONUtil.parseObject("[\"t1\",\"t2\"]", new TypeReference[(String, String)] {})
  print(t)
  val m = JSONUtil.parseObject("{\"i\":1,\"j\":\"j\"}", new TypeReference[Map[String, _]] {})
  println(m)
  val l = JSONUtil.parseObject("[\"t1\",\"t2\"]", new TypeReference[Seq[String]] {})
  println(l)

  println(JSONUtil.toJsonString(test))
  println(JSONUtil.toJsonString(map))
  println(JSONUtil.toJsonString(map1))
  println(JSONUtil.toJsonString(tuple))
}
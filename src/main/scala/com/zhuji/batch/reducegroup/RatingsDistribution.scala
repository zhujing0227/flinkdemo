package com.zhuji.batch.reducegroup

import java.lang

import com.zhuji.util.JSONUtil
import org.apache.flink.api.common.functions.{GroupCombineFunction, GroupReduceFunction}
import org.apache.flink.api.common.operators.Order
import org.apache.flink.api.scala.{ExecutionEnvironment, _}
import org.apache.flink.util.Collector

import scala.beans.BeanProperty
import scala.collection.JavaConversions._

/**
  * @author zhuji on 2019-01-21
  */

/**
  * 统计每部电影的评分总数
  */
object RatingsDistribution {

  def main(args: Array[String]): Unit = {
    val env = ExecutionEnvironment.getExecutionEnvironment
    val ratings = env.readCsvFile[Rating](
      "src/main/resources/csv/ratings.csv",
      ignoreFirstLine = true)
    ratings
      .groupBy("movieId")
      .reduceGroup(iter => {
        val list = iter.toList
        (list.head.getMovieId, list.map(_.getRating).sum)
      })
      //      .sum("rating")
      .print

  }

}

case class Rating(@BeanProperty userId: Int,
                  @BeanProperty movieId: Int,
                  @BeanProperty rating: Float,
                  @BeanProperty timestamp: Long){
  override def toString: String = JSONUtil.toJsonString(this)
}

object RatingsDistributionWithCombineReduce {
  def main(args: Array[String]): Unit = {
    val env = ExecutionEnvironment.getExecutionEnvironment
    val ratings = env.readCsvFile[Rating](
      "src/main/resources/csv/ratings.csv",
      ignoreFirstLine = true)
    ratings
      .groupBy("movieId")
      //combine
      .combineGroup((values, out: Collector[(Int, Float)]) => {
      val list = values.toList
      out.collect(list.head.getMovieId, list.map(_.getRating).sum)
    })
      .groupBy(_._1)
      //reduce
      .reduceGroup(iter => {
      val list = iter.toList
      (list.head._1, list.map(_._2).sum)
    })
      .print
  }
}

/**
  * 统计每部电影的评分平均值
  */
object RatingsDistributionWithCombine {
  def main(args: Array[String]): Unit = {
    val env = ExecutionEnvironment.getExecutionEnvironment
    val ratings = env.readCsvFile[Rating](
      "src/main/resources/csv/ratings.csv",
      ignoreFirstLine = true)
    ratings
      .groupBy("movieId")
      .reduceGroup(new CombineGroupReducer)
      .map(a => (a._1, a._3 / a._2))
      .sortPartition(_._2, Order.DESCENDING)
      .print
  }

  /**
    * combine和reduce的输入输出需要保持一致
    */
  class CombineGroupReducer extends GroupReduceFunction[Rating, (Int, Int, Float)]
    with GroupCombineFunction[Rating, (Int, Int, Float)] {
    override def reduce(values: lang.Iterable[Rating], out: Collector[(Int, Int, Float)]): Unit = {
      val list = values.toList
      out.collect(list.head.getMovieId, list.size, list.map(_.getRating).sum)
    }

    override def combine(values: lang.Iterable[Rating], out: Collector[(Int, Int, Float)]): Unit = {
      val list = values.toList
      out.collect(list.head.getMovieId, list.size, list.map(_.getRating).sum)
    }
  }

}
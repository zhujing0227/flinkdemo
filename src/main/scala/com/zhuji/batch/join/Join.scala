package com.zhuji.batch.join

import com.zhuji.batch.join.Join.Tag
import com.zhuji.batch.reducegroup.Rating
import com.zhuji.util.JSONUtil
import org.apache.flink.api.common.operators.Order
import org.apache.flink.api.scala.{ExecutionEnvironment, _}
import org.apache.flink.core.fs.FileSystem.WriteMode

import scala.beans.BeanProperty

/**
  * @author zhuji on 2019/1/22
  */

/**
  * 找出评分均值前10的标签
  */
object Join {
  def main(args: Array[String]): Unit = {
    val env = ExecutionEnvironment.getExecutionEnvironment
    val ratings = env.readCsvFile[Rating](
      "/Users/zhuji/flinkdemo/src/main/resources/csv/ratings.csv",
      ignoreFirstLine = true)
      .first(10000)
    val tags = env.readCsvFile[Tag](
      "/Users/zhuji/flinkdemo/src/main/resources/csv/tags.csv",
      lenient = true,
      ignoreFirstLine = true)
      .filter(_.tag != null)
      .first(10000)

    ratings.join(tags)
      .where(_.movieId)
      .equalTo(_.movieId)
      .apply((rating, tag) => (tag.tag, rating))
      .groupBy(_._1)
      .reduceGroup(values => {
        val list = values.toList
        (list.head._1, list.map(_._2.rating).sum / list.size)
      }).filter(_._1 != null)
      .sortPartition(_._2, Order.DESCENDING)
      .first(10)
      .print

    ratings.writeAsText("hdfs://localhost:9000/usr/hive/wirehome/ratings1.txt", WriteMode.NO_OVERWRITE)

    env.execute()
  }

  case class Tag(@BeanProperty userId: Int,
                 @BeanProperty movieId: Int,
                 @BeanProperty tag: String,
                 @BeanProperty timestamp: Long) {
    override def toString: String = JSONUtil.toJsonString(this)
  }

}

/**
  * coGroup实现 leftOuterJoin
  */
object LeftJoin {
  def main(args: Array[String]): Unit = {
    val env = ExecutionEnvironment.getExecutionEnvironment
    val ratings = env.readCsvFile[Rating](
      "src/main/resources/csv/ratings.csv",
      ignoreFirstLine = true)
      .first(10000)
    val tags = env.readCsvFile[Tag](
      "src/main/resources/csv/tags.csv",
      lenient = true,
      ignoreFirstLine = true)
      .filter(_.tag != null)
      .first(10000)

    ratings.coGroup(tags)
      .where(_.movieId)
      .equalTo(_.movieId)
      .reduceGroup(iter => {
        iter.flatMap(a => {
          val r = a._1
          val t = a._2
          r.map(r => {
            (t.find(t => r.movieId == t.movieId), r)
          }).filter(_._1.isDefined)
            .map(a => (a._1.get.tag, a._2))
        })
      }).flatMap(x => x)
      .groupBy(_._1)
      .reduceGroup(values => {
        val list = values.toList
        (list.head._1, list.map(_._2.rating).sum / list.size)
      })
      .sortPartition(_._2, Order.DESCENDING)
      .first(10)
      .print

  }
}